<?php
session_start(); 
if(isset($_SESSION['uname'])){  header("Location: home.php"); exit(); 
} else{ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>::Daily Income & Cost Management::</title>
            <script type="text/javascript" src="js/script.js"> </script>
            <script type="text/javascript" src="js/masked_input_1.js"></script> 
            <script type="text/javascript" src="js/masked_input_ex.js"></script>
            <script type="text/ecmascript"> </script>
            <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
<script type="text/javascript"> 
    function valid_form_login(){
		var error="";
		//Validation Code for login form
			var f=document.forms["login_form"].uname.value;
			if(f<=0){
					error=error+'Please enter your Username or Email. <br>';	
					document.forms["login_form"].uname.focus();
			}
			var g=document.forms["login_form"].pw.value;
			if(g<=0){
					error=error+'Please enter your Password.';	
					document.forms["login_form"].pw.focus();
			}
			if(error){
					//alert(error);
					document.getElementById("valid_form").innerHTML = error;
				return false;
			}
			return true;
	}
  </script>
            <div id="container_admin" class="wrap">
            <div id="login_logo"><img src="library/images/logo.png"/></div>
            <div id="valid_form"> </div>
    <form id="login_form" name="login_form"  method="post" action="login.php" onsubmit="return valid_form_login();">
            <div id="log_in"><label for="user"> Username : </label>
                <input type="text" id="user" name="uname" placeholder="Username or Email." class="requiredField"/>
            </div>
            <div id="log_in"><label for="pass"> Password : </label>
                    <input type="password" id="pass" name="pw" placeholder="Enter Password." class="requiredField"/>
            </div>
            <div id="log_in">
                    <input type="hidden" name="submitted" id="submitted" value="true" />
                    <button type="submit" id="submitbutton" name="submit" value="submit"  class="submitbutton">Login</button>
            </div>
    </form>
<?php } ?>