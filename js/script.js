//determine user's display resolution
/*
 var viewportwidth;
 var viewportheight;
  
 // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
  
 if (typeof window.innerWidth != 'undefined')
 {
      viewportwidth = window.innerWidth,
      viewportheight = window.innerHeight
 }
  
// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
 
 else if (typeof document.documentElement != 'undefined'
     && typeof document.documentElement.clientWidth !=
     'undefined' && document.documentElement.clientWidth != 0)
 {
       viewportwidth = document.documentElement.clientWidth,
       viewportheight = document.documentElement.clientHeight
 }
  
 // older versions of IE
  
 else
 {
       viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
       viewportheight = document.getElementsByTagName('body')[0].clientHeight
 }
document.write('<p>Your viewport width is '+viewportwidth+'x'+viewportheight+'</p>');

*/


//the function for on submit blank field checking
	
function valid_form(){
		
		var error="";
			var a=document.forms["trans"].type.value;
			if(a<=0){
					error=error+'Please select Transaction Type.<br>';	
					document.forms["trans"].type.focus();
			}
			
			var b=document.forms["trans"].tdate.value;
			if(b<=0){
					error=error+'Please insert the date.<br>';	
					document.forms["trans"].tdate.focus();
					
			}
			var c=document.forms["trans"].particulars.value;
			if(c<=0){
					error=error+'Please type the transaction particulars.<br>';	
					document.forms["trans"].particulars.focus();
			}
			
			var d=document.forms["trans"].amount.value;
			if(d<=0){
					error=error+'Please enter the transaction amount.<br>';	
					document.forms["trans"].amount.focus();
			}
			
			var e=document.forms["trans"].narp.value;
			if(e<=0){
					error=error+'Please enter Receiver/Payer ID or Name.<br>';	
					document.forms["trans"].narp.focus();
			}
			
			
			
			
		    if(error){
					
					//alert(error);
					document.getElementById("valid_form").innerHTML = error;
					return false;
			
			}
			
			return true;
	}
	
	
/* statement date submission form validation */
function state_form(){
		
		var error="";
			
			
			var a=document.forms["trans"].fdate.value;
			if(a<=0){
					error=error+'Please insert the From date.<br>';	
					document.forms["trans"].fdate.focus();
					
			
			}
			
			
			var b=document.forms["trans"].tdate.value;
			if(b<=0){
					error=error+'Please insert the To date.<br>';	
					document.forms["trans"].tdate.focus();
					
			
			}
			var c=document.forms["trans"].type.value;
			if(c<=0){
					error=error+'Please select type.<br>';	
					document.forms["trans"].type.focus();
					
			
			}
			
			
			
		    if(error){
					
					//alert(error);
					document.getElementById("valid_form").innerHTML = error;
					return false;
			
			}
			
			return true;
	}