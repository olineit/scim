<?php
session_start(); 
if(isset($_SESSION['uname'])){  header("Location: logged_in.php"); exit(); 
} else{?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>::Daily Income & Cost Management::</title>
	<script type="text/javascript" src="js/script.js"> </script>
   	<script type="text/javascript" src="js/masked_input_1.js"></script> 
	<script type="text/javascript" src="js/masked_input_ex.js"></script>
<style>
.login {
    float:left;
	margin:2px;
    padding: 3px;
    width:100%;
    height: auto;
	border: 1px solid black;
	text-align:center;
    
}
</style>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<script type="text/javascript"> 
function valid_form_login(){
		
		var error="";
		
		//Validation Code for login form
			
			var f=document.forms["login_form"].uname.value;
			if(f<=0){
					error=error+'Please enter your Username or Email. <br>';	
					document.forms["login_form"].uname.focus();
			}
			
			var g=document.forms["login_form"].pw.value;
			if(g<=0){
					error=error+'Please enter your Password.';	
					document.forms["login_form"].pw.focus();
			}
			
		    if(error){
					
					//alert(error);
					document.getElementById("valid_form").innerHTML = error;
				return false;
			
			}
			
			return true;
			
	}</script>
<div class="login" id="valid_form"  style="color:#F00;"> User Login </div>


<form name="login_form"  method="post" action="login.php" onsubmit="return valid_form_login();">
<div class="login">
<label>Username :</label>
    <input type="text" id="user" name="uname" placeholder="Username or Email."/>
</div>
<div class="login">
<label>Passwoord :</label>
<input type="password" id="pass" name="pw" placeholder="Enter Password." />
</div>
<div class="login">
 <input type="submit" name="submit" value="Login"/>
</div>
</form>

</body>
</html>

<?php } ?>